package com.roots.rootsapp.utils;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * Created by suhaibroomy on 20/11/16.
 */

public class Utility {

    public static String jsonify(Object object) {
        return new Gson().toJson(object);
    }

    public static <T> T objectify(String pJson, Class<T> pType) {
        if (pJson == null || pJson.trim().length() == 0) {
            return null;
        }
        try {
            return new Gson().fromJson(pJson, pType);
        } catch (Exception ignored) {
        }
        return null;
    }
}
