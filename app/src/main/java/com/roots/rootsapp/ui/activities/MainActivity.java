package com.roots.rootsapp.ui.activities;

import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.roots.rootsapp.R;
import com.roots.rootsapp.ui.fragments.ExposerFragment;
import com.roots.rootsapp.ui.fragments.NotificationFragment;
import com.roots.rootsapp.ui.fragments.RecentsFragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabSelectedListener;

public class MainActivity extends AppCompatActivity {


  private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.three_buttons_activity);
                BottomBar bottomBar = BottomBar.attach(this, savedInstanceState);

                        bottomBar.setItemsFromMenu(R.menu.three_buttons_menu, new OnMenuTabSelectedListener() {
                               @Override
                               public void onMenuItemSelected(int itemId) {
                                       Fragment fragment = null;
                                       switch (itemId) {
                                                case R.id.recent_item:
                                                        fragment = new RecentsFragment();
                                                        break;
                                             case R.id.exposer_item:
                                                        fragment = new ExposerFragment();
                                                        break;
                                             case R.id.notif_item:
                                                    fragment = new NotificationFragment();
                                                    break;
                                            }
                                     if (fragment != null) {
                                            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                                            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

                                        }
                                    }
                         });

                // Set the color for the active tab. Ignored on mobile when there are more than three tabs.
                bottomBar.setActiveTabColor("#C2185B");
    }
}