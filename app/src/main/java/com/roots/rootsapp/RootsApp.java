package com.roots.rootsapp;

import android.app.Application;

/**
 * Created by suhaibroomy on 10/11/16.
 */

public class RootsApp extends Application {

    public static RootsApp getInstance() {
        return _instance;
    }

    private static RootsApp _instance;

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
    }
}
