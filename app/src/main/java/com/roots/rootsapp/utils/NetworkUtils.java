package com.roots.rootsapp.utils;

import com.google.gson.JsonElement;
import com.roots.rootsapp.RootsApp;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by suhaibroomy on 15/10/16.
 * This class acts as a wrapper for any mechanism used for
 * interacting with server.
 */

public class NetworkUtils {
    //TODO implement retrofit here. You can make a init method which would initialize retrofit and other helper methods later on like getFeedFromServer

    private static Retrofit _retrofit;

    private static final int DEFAULT_READ_TIME_OUT = 20;
    private static final int DEFAULT_CONNECT_TIME_OUT = 20;

    private static void init() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(DEFAULT_READ_TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(DEFAULT_CONNECT_TIME_OUT, TimeUnit.SECONDS)
                .cache(new Cache(RootsApp.getInstance().getExternalCacheDir(), 10485760L)).build();
        _retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(Constant.API.API_END_POINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static <T> void get(String appendUrl, final Callbacks<T> callbacks, final Class<T> tClass) {
        if (_retrofit == null) {
            init();
        }

        APICall apiService = _retrofit.create(APICall.class);
        Call<JsonElement> responseClass = apiService.getRequest(appendUrl);
        responseClass.enqueue(new Callback<JsonElement>() {
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    callbacks.onSuccess(Utility.objectify(Utility.jsonify(response.body()), tClass));
                } else {
                    try {
                        callbacks.onError(response.code(), response.errorBody().string());
                        response.errorBody().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            public void onFailure(Call<JsonElement> call, Throwable t) {
                callbacks.onFailure(t);
            }
        });
    }

    private interface APICall {
        @GET
        Call<JsonElement> getRequest(@Url String url);
    }

    public interface Callbacks<T> {
        void onSuccess(T t);

        void onError(int errorCode, String errorJson);

        void onFailure(Throwable var1);
    }
}
